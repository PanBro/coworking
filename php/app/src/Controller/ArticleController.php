<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing;
use Symfony\Component\HttpFoundation\Response;

class ArticleController extends AbstractController
{
    /**
     * @Routing\Annotation\Route("/", name="app_homepage")
     */
    public function homepage()
    {
        $heading ='Последние новости';
        $title ='Коворкинг твоей мечты';

//        return new Response('it\'s homepage');
//        return new $this->render('main.html.twig', []);
        return $this->render('main.html.twig', [
            'heading' => $heading,
            'title' =>$title,

        ]);
    }

    /**
     * @Routing\Annotation\Route("/review", name="reviews_page")
     */
    public function reviews()
    {
        $heading ='Отзывы';
        $title ='Отзывы. Коворкинг твоей мечты';

        return $this->render('reviews.html.twig', [
            'heading' => $heading,
            'title' =>$title,

        ]);
    }
//63342
    /**
     * @Routing\Annotation\Route("/prices", name="prices_page")
     */
    public function prices()
    {
        $heading ='Тарифы';
        $title = 'Тарифы. Коворкинг твоей мечты';

        return $this->render('prices.html.twig', [
            'heading' => $heading,
            'title' =>$title
        ]);
    }
//
//    /**
//     * @Routing\Annotation\Route("/{slug}", name="slug");
//     */
//    public function show($slug)
//    {
//
//        $heading ='Последние новости';
////        return new Response('it\'s homepage');
////        return new $this->render('main.html.twig', []);
//        return $this->render('reviews.html.twig', [
//            'heading' => $heading,
//            'slug' => $slug,
//        ]);



// @Routing\Annotation\Route("/sth/{slug}")

//    /**
//     * @Routing\Annotation\Route("/")
//     */
////    public function show($slug)
//    public function show()
//    {
//        $comments = [
//            'The moon handles.',
//             'True enlightenments invents most densities.',
//             'honorable ascensions experiences most affirmations. solitude',
//             'be outer. Be unprepared.',
//        ];
//
//        return $this->render('main.html.twig', [
////            'title' => $slug,
//            'comments' => $comments,
//        ]);
//    }



//        return new Response(sprintf(
//            'Future page to show the article: %s',
//            $slug
//        ));
}