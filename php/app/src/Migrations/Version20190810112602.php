<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190810112602 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE options (id VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, value VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, comment VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, type VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE reviews (id INT AUTO_INCREMENT NOT NULL, date DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, pic VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, text VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, link VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, display_pic TINYINT(1) NOT NULL, hidden TINYINT(1) DEFAULT \'0\' NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE tariff_pics (id INT AUTO_INCREMENT NOT NULL, tariff_id INT NOT NULL, link VARCHAR(225) NOT NULL COLLATE utf8mb4_unicode_ci, INDEX tariff_id (tariff_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE tariffs (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, pic VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, cost_month DOUBLE PRECISION DEFAULT NULL, cost_day DOUBLE PRECISION DEFAULT NULL, cost_hour DOUBLE PRECISION DEFAULT NULL, cost_individual DOUBLE PRECISION DEFAULT NULL, description INT DEFAULT NULL, short_desc INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE tariffs_top (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, content TEXT NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE tariff_pics ADD CONSTRAINT tariff_pics_ibfk_1 FOREIGN KEY (tariff_id) REFERENCES tariffs (id)');
    }

    public function down(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tariff_pics DROP FOREIGN KEY tariff_pics_ibfk_1');
        $this->addSql('DROP TABLE options');
        $this->addSql('DROP TABLE reviews');
        $this->addSql('DROP TABLE tariff_pics');
        $this->addSql('DROP TABLE tariffs');
        $this->addSql('DROP TABLE tariffs_top');
    }

}
