.PHONY: dev-up

dev-up:
	@ docker-compose up -d
	@ docker-compose run --rm php composer.phar install
	@ docker-compose run --rm php bin/console doctrine:migrations:migrate

dev-build:
	@ docker-compose down
	@ docker volume prune -f
	@ docker-compose up -d --build --remove-orphans
	@ make dev-up

dev-hardreset:
	@ docker-compose down
	@ sudo rm -r mysql/*
	@ docker system prune -af
	@ make dev-build

dev-restart:
	@ docker-compose restart

dev-down:
	@ docker-compose down